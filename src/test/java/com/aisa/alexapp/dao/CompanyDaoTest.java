package com.aisa.alexapp.dao;

import com.aisa.alexapp.backend.dao.CompanyDAO;
import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("/application-test.properties")
@Sql(value = {"/companies-before.sql", "/users-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/users-after.sql", "/companies-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class CompanyDaoTest {
    @Autowired
    private CompanyDAO companyRepository;

    private final int COMPANIES_LENGTH = 3;

    @Test
    public void testGetAllCompanies() {
        List<Company> list = companyRepository.getAllCompanies();
        Assert.assertEquals(list.size(), COMPANIES_LENGTH);
    }

    @Test
    public void testSearchCompanyByName() {
        List<Company> list = companyRepository.searchCompanyByName("");
        List<Company> listLower = companyRepository.searchCompanyByName("s");
        List<Company> listUpper = companyRepository.searchCompanyByName("S");
        List<Company> listEmpty = companyRepository.searchCompanyByName("adasdadasdadasdasdasdas");

        final int foundQuantity = 2;

        Assert.assertEquals(listLower.size(), listUpper.size());
        Assert.assertEquals(listLower.size(), foundQuantity);
        Assert.assertEquals(listEmpty.size(), 0);
        Assert.assertEquals(list.size(), COMPANIES_LENGTH);
    }

    @Test
    public void testFindById() {
        final Long companyId = companyRepository.getAllCompanies().get(0).getId();
        final String companyName = "Last company";

        Company company = companyRepository.findById(companyId);

        Assert.assertNotNull(company);
        Assert.assertEquals(company.getCompanyName(), companyName);
        Assert.assertNull(companyRepository.findById(0L));
    }

    @Test
    @Transactional
    @Rollback()
    public void testAddCompany() {
        Company company = new Company();

        company.setCompanyName("Test Company");
        company.setAddress("123");
        company.setPhone("+7213123");
        company.setTaxNumber("1231231212");

        boolean status = companyRepository.addCompany(company);

        Assert.assertTrue(status);
    }

    @Test
    @Transactional
    @Rollback()
    public void testDeleteCompany() {
        final Long companyId = companyRepository.getAllCompanies().get(0).getId();
        final Long companyConnectedId = companyRepository.getAllCompanies().get(1).getId();
        Company company = new Company();

        company.setId(companyId);
        company.setCompanyName("12312");
        company.setAddress("123");
        company.setPhone("+7213123123");
        company.setTaxNumber("1231231212");

        boolean status_true = companyRepository.deleteCompany(company);

        Assert.assertTrue(status_true);

        company.setId(0L);

        boolean status_false = companyRepository.deleteCompany(company);

        Assert.assertFalse(status_false);

        company.setId(companyConnectedId);

        status_false = companyRepository.deleteCompany(company);

        Assert.assertTrue(status_false);
    }

    @Test
    @Transactional
    @Rollback()
    public void testUpdateCompany() {
        final Long companyId = companyRepository.getAllCompanies().get(0).getId();
        Company company = new Company();

        company.setId(companyId);
        company.setCompanyName("12312");
        company.setAddress("123");
        company.setPhone("+7213123123");
        company.setTaxNumber("1231231212");

        boolean status_true = companyRepository.updateCompany(company);

        Assert.assertTrue(status_true);

        company.setId(0L);

        boolean status_false = companyRepository.deleteCompany(company);

        Assert.assertFalse(status_false);
    }

    @Test
    public void testGetUsersByCompanyId() {
        final Long companyId = companyRepository.getAllCompanies().get(COMPANIES_LENGTH - 1).getId();
        final int resultSize = 2;

        List<User> list = companyRepository.getUsersByCompanyId(companyId);
        List<User> listEmpty = companyRepository.getUsersByCompanyId(0L);

        Assert.assertEquals(list.size(), resultSize);
        Assert.assertEquals(listEmpty.size(), 0);
    }

}
