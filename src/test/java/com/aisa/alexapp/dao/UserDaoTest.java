package com.aisa.alexapp.dao;

import com.aisa.alexapp.backend.dao.UserDAO;
import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("/application-test.properties")
@Sql(value = {"/companies-before.sql", "/users-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/users-after.sql", "/companies-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UserDaoTest {
    @Autowired
    private UserDAO userRepository;

    private final int USERS_LENGTH = 3;

    @Test
    public void testGetAllUsers() {
        List<User> list = userRepository.getAllUsers();
        Assert.assertEquals(list.size(), USERS_LENGTH);
    }

    @Test
    public void testSearchUsersByName() {
        List<User> listLower = userRepository.searchUsersByName("employee");
        List<User> listUpper = userRepository.searchUsersByName("Employee");
        List<User> listEmpty = userRepository.searchUsersByName("adasdadasdadasdasdasdas");

        final int foundQuantity = 2;

        Assert.assertEquals(listLower.size(), listUpper.size());
        Assert.assertEquals(listLower.size(), foundQuantity);
        Assert.assertEquals(listEmpty.size(), 0);
    }

    @Test
    public void testFindById() {
        final Long userId = userRepository.getAllUsers().get(0).getId();
        final String userName = "Not";

        User user = userRepository.findById(userId);

        Assert.assertNotNull(user);
        Assert.assertEquals(user.getFirstName(), userName);
        Assert.assertNull(userRepository.findById(0L));
    }

    @Test
    @Transactional
    @Rollback()
    public void testAddUser() {
        User user = new User();
        final Long companyId = userRepository.getAllUsers().get(0).getCompanyId();

        user.setFirstName("Test");
        user.setLastName("Test");
        user.setBirthDate(LocalDate.now());
        user.setMidName("Mid");
        user.setEmail("some@sad.asd");
        user.setCompanyId(companyId);

        boolean status = userRepository.addUser(user);

        Assert.assertTrue(status);
    }

    @Test
    @Transactional
    @Rollback()
    public void testDeleteUser() {
        final Long userId = userRepository.getAllUsers().get(0).getId();
        User user = new User();

        user.setId(userId);
        user.setFirstName("Test");
        user.setLastName("Test");
        user.setBirthDate(LocalDate.now());
        user.setMidName("Mid");
        user.setEmail("some@sad.asd");

        boolean status_true = userRepository.deleteUser(user);

        Assert.assertTrue(status_true);

        user.setId(0L);

        boolean status_false = userRepository.deleteUser(user);

        Assert.assertFalse(status_false);
    }

    @Test
    @Transactional
    @Rollback()
    public void testUpdateUser() {
        final Long userId = userRepository.getAllUsers().get(0).getId();
        User user = new User();

        user.setId(userId);
        user.setFirstName("Test");
        user.setLastName("Test");
        user.setBirthDate(LocalDate.now());
        user.setMidName("Mid");
        user.setEmail("some@sad.asd");

        boolean status_true = userRepository.updateUser(user);

        Assert.assertTrue(status_true);

        user.setId(0L);

        boolean status_false = userRepository.updateUser(user);

        Assert.assertFalse(status_false);
    }
}
