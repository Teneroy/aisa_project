package com.aisa.alexapp.service;

import com.aisa.alexapp.backend.dao.CompanyDAO;
import com.aisa.alexapp.backend.dao.UserDAO;
import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;
import com.aisa.alexapp.backend.services.CompanyService;
import com.aisa.alexapp.backend.services.UserService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("/application-test.properties")
public class UserServiceTest {

    @Mock
    private UserDAO userDAO;

    @InjectMocks
    private UserService userService;

    @Test(expected = IllegalArgumentException.class)
    public void when_add_user_without_first_name_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);

        userService.addUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_add_user_without_last_name_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        //user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");

        userService.addUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_add_user_without_email_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        //user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");

        userService.addUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_add_user_without_company_id_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        //user.setCompanyId(1L);
        user.setFirstName("asd");

        userService.addUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_add_user_without_birthDate_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        //user.setBirthDate(new Date());
        user.setCompanyId(1L);
        user.setFirstName("asd");

        userService.addUser(user);
    }

    @Test
    public void when_add_user_without_midname_return_true() {
        User user = new User();
        //user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");

        when(userService.addUser(user)).thenReturn(true);

        Assert.assertTrue(userService.addUser(user));
    }

    @Test
    public void when_add_user_without_company_name_return_true() {
        User user = new User();
        user.setMidName("asd");
        //user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");

        when(userService.addUser(user)).thenReturn(true);

        Assert.assertTrue(userService.addUser(user));
    }

    @Test
    public void when_add_user_with_valid_parameters_return_true() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");

        when(userService.addUser(user)).thenReturn(true);

        Assert.assertTrue(userService.addUser(user));
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_delete_user_without_id_throw_exception() {
        User user = new User();
        user.setCompanyId(1L);

        userService.deleteUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_delete_user_with_negative_id_throw_exception() {
        User user = new User();
        user.setId(-1L);

        userService.deleteUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_delete_user_with_id_equals_zero_throw_exception() {
        User user = new User();
        user.setId(0L);

        userService.deleteUser(user);
    }

    @Test
    public void when_delete_user_with_valid_parameters_return_true() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setId(1L);

        when(userService.deleteUser(user)).thenReturn(true);

        Assert.assertTrue(userService.deleteUser(user));
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_user_without_id_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setFirstName("asd");
        user.setCompanyId(1L);

        userService.updateUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_user_with_negative_id_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(-1L);

        userService.updateUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_user_with_id_equals_zero_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(0L);

        userService.updateUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_user_without_first_name_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(0L);

        userService.updateUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_user_without_last_name_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        //user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(1L);

        userService.updateUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_user_without_email_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        //user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(1L);

        userService.updateUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_without_company_id_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        //user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(1L);

        userService.updateUser(user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_without_birthDate_throw_exception() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        //user.setBirthDate(new Date());
        user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(1L);

        userService.updateUser(user);
    }

    @Test
    public void when_update_user_without_midname_return_true() {
        User user = new User();
        //user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(1L);

        when(userService.updateUser(user)).thenReturn(true);

        Assert.assertTrue(userService.updateUser(user));
    }

    @Test
    public void when_update_user_without_company_name_return_true() {
        User user = new User();
        user.setMidName("asd");
        //user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(1L);

        when(userService.updateUser(user)).thenReturn(true);

        Assert.assertTrue(userService.updateUser(user));
    }

    @Test
    public void when_update_user_with_valid_parameters_return_true() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(1L);

        when(userService.updateUser(user)).thenReturn(true);

        Assert.assertTrue(userService.updateUser(user));
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_call_findById_with_negative_id_throw_exception() {
        userService.findById(-1L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_call_findById_with_zero_id_throw_exception() {
        userService.findById(0L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_call_findById_with_null_id_throw_exception() {
        userService.findById(null);
    }

    @Test
    public void when_call_findById_with_valid_id_return_company() {
        User user = new User();
        user.setMidName("asd");
        user.setCompanyName("asd");
        user.setEmail("asd");
        user.setLastName("adas");
        user.setBirthDate(LocalDate.now());
        user.setCompanyId(1L);
        user.setFirstName("asd");
        user.setId(1L);

        when(userDAO.findById(user.getId())).thenReturn(user);

        Assert.assertEquals(userService.findById(user.getId()), user);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_search_user_with_null_parameter_then_throw_exception() {
        userService.searchUsersByName(null);
    }

    @Test
    public void when_search_user_with_valid_parameter_then_return_company_list() {
        List<User> users = new ArrayList<>();

        users.add(new User(1L, "asdas", "asdas", "asdas", "asdasd", 1L, "asdas", LocalDate.now()));
        users.add(new User(2L, "asdas", "asdas", "asdas", "asdasd", 1L, "asdas", LocalDate.now()));

        String name = "s";

        when(userDAO.searchUsersByName(name)).thenReturn(users);

        Assert.assertEquals(userService.searchUsersByName(name).size(), users.size());
    }

}
