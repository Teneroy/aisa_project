package com.aisa.alexapp.service;

import com.aisa.alexapp.backend.dao.CompanyDAO;
import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;
import com.aisa.alexapp.backend.services.CompanyService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource("/application-test.properties")
public class CompanyServiceTest {

    @Mock
    private CompanyDAO companyDAO;

    @InjectMocks
    private CompanyService companyService;

    @Test
    public void testGetAllCompanies() {
        List<Company> list = new ArrayList<>();
        list.add(new Company(1L, "1231231212", "+7312", "asdas", "Comp"));
        list.add(new Company(2L, "1231231212", "+731212", "asd123as", "Comp2"));

        when(companyDAO.getAllCompanies()).thenReturn(list);

        Assert.assertEquals(companyService.getAllCompanies().size(), 2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_add_company_without_company_name_throw_exception() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        company.setPhone("+7213");
        company.setAddress("asdas");
        company.setId(1L);
        //company.setCompanyName("ASd");

        companyService.addCompany(company);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_add_company_without_tax_number_throw_exception() {
        Company company = new Company();
        //company.setTaxNumber(123L);
        company.setPhone("+7213");
        company.setAddress("asdas");
        company.setId(1L);
        company.setCompanyName("ASd");

        companyService.addCompany(company);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_add_company_without_phone_throw_exception() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        //company.setPhone("+7213");
        company.setAddress("asdas");
        company.setId(1L);
        company.setCompanyName("ASd");

        companyService.addCompany(company);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_add_company_without_address_throw_exception() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        company.setPhone("+7213");
        //company.setAddress("asdas");
        company.setId(1L);
        company.setCompanyName("ASd");

        companyService.addCompany(company);
    }

//    @Test(expected = IllegalArgumentException.class)
//    public void when_add_company_with_negative_tax_number_throw_exception() {
//        Company company = new Company();
//        company.setTaxNumber("1231231212");
//        company.setPhone("+7213");
//        company.setAddress("asdas");
//        company.setId(1L);
//        company.setCompanyName("ASd");
//
//        companyService.addCompany(company);
//    }

    @Test
    public void when_add_company_with_valid_params_true() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        company.setPhone("+7213");
        company.setAddress("asdas");
        company.setCompanyName("ASd");

        when(companyDAO.addCompany(company)).thenReturn(true);

        Assert.assertTrue(companyService.addCompany(company));
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_delete_company_without_id_throw_exception() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        company.setPhone("+7213");
        company.setAddress("asdas");
        //company.setId(1L);
        company.setCompanyName("ASd");

        companyService.deleteCompany(company);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_delete_company_with_negative_id_throw_exception() {
        Company company = new Company();
        company.setId(-1L);

        companyService.deleteCompany(company);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_delete_company_with_id_equals_zero_throw_exception() {
        Company company = new Company();
        company.setId(0L);
        company.setCompanyName("ASd");

        companyService.deleteCompany(company);
    }

    @Test
    public void when_delete_company_with_valid_params_true() {
        Company company = new Company();
        company.setId(1L);

        when(companyDAO.deleteCompany(company)).thenReturn(true);

        Assert.assertTrue(companyService.deleteCompany(company));
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_company_without_company_name_throw_exception() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        company.setPhone("+7213");
        company.setAddress("asdas");
        company.setId(1L);
        //company.setCompanyName("ASd");

        companyService.updateCompany(company);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_company_without_tax_number_throw_exception() {
        Company company = new Company();
        //company.setTaxNumber(123L);
        company.setPhone("+7213");
        company.setAddress("asdas");
        company.setId(1L);
        company.setCompanyName("ASd");

        companyService.updateCompany(company);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_company_without_phone_throw_exception() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        //company.setPhone("+7213");
        company.setAddress("asdas");
        company.setId(1L);
        company.setCompanyName("ASd");

        companyService.updateCompany(company);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_company_without_address_throw_exception() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        company.setPhone("+7213");
        //company.setAddress("asdas");
        company.setId(1L);
        company.setCompanyName("ASd");

        companyService.updateCompany(company);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_update_company_without_id_throw_exception() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        company.setPhone("+7213");
        company.setAddress("asdas");
        //company.setId(1L);
        company.setCompanyName("ASd");

        companyService.updateCompany(company);
    }

//    @Test(expected = IllegalArgumentException.class)
//    public void when_update_company_with_negative_tax_number_throw_exception() {
//        Company company = new Company();
//        company.setTaxNumber(-123L);
//        company.setPhone("+7213");
//        company.setAddress("asdas");
//        company.setId(1L);
//        company.setCompanyName("ASd");
//
//        companyService.updateCompany(company);
//    }

    @Test
    public void when_update_company_with_valid_params_true() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        company.setPhone("+7213");
        company.setAddress("asdas");
        company.setCompanyName("ASd");
        company.setId(1L);

        when(companyDAO.updateCompany(company)).thenReturn(true);

        Assert.assertTrue(companyService.updateCompany(company));
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_call_findById_with_negative_id_throw_exception() {
        companyService.findById(-1L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_call_findById_with_zero_id_throw_exception() {
        companyService.findById(0L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_call_findById_with_null_id_throw_exception() {
        companyService.findById(null);
    }

    @Test
    public void when_call_findById_with_valid_id_return_company() {
        Company company = new Company();
        company.setTaxNumber("1231231212");
        company.setPhone("+7213");
        company.setAddress("asdas");
        company.setCompanyName("ASd");
        company.setId(1L);

        when(companyDAO.findById(company.getId())).thenReturn(company);

        Assert.assertEquals(companyService.findById(company.getId()), company);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_call_getUsersByCompanyId_with_negative_id_throw_exception() {
        companyService.getUsersByCompanyId(-1L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_call_getUsersByCompanyId_with_zero_id_throw_exception() {
        companyService.getUsersByCompanyId(0L);
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_call_getUsersByCompanyId_with_null_id_throw_exception() {
        companyService.getUsersByCompanyId(null);
    }

    @Test
    public void when_call_getUsersByCompanyId_with_valid_id_return_list_of_users() {
        Company company = new Company(1L, "1231231212", "+7213", "asdas", "ASd");

        List<User> users = new ArrayList<>();

        users.add(new User(1L, "asdas", "asdas", "asdas", "asdasd", 1L, "asdas", LocalDate.now()));
        users.add(new User(2L, "asdas", "asdas", "asdas", "asdasd", 1L, "asdas", LocalDate.now()));

        when(companyDAO.getUsersByCompanyId(company.getId())).thenReturn(users);

        Assert.assertEquals(companyService.getUsersByCompanyId(company.getId()).size(), users.size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void when_search_company_with_null_parameter_then_throw_exception() {
        companyService.searchCompanyByName(null);
    }

    @Test
    public void when_search_company_with_valid_parameter_then_return_company_list() {
        List<Company> list = new ArrayList<>();

        list.add(new Company(1L, "1231231212", "+7213", "asdas", "ASd"));
        list.add(new Company(2L, "1231231212", "+72a13", "adsdas", "ASad"));

        String name = "s";

        when(companyDAO.searchCompanyByName(name)).thenReturn(list);

        Assert.assertEquals(companyService.searchCompanyByName(name).size(), list.size());
    }
}
