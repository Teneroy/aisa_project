INSERT INTO companies(companyname, taxnumber, phone, address) VALUES ('AISA IT-SERVICE', '123123123', '+792154312', 'Moscow&Spb');
INSERT INTO companies(companyname, taxnumber, phone, address) VALUES ('Not Google', '123123223', '+3423423432423', 'LA');
INSERT INTO companies(companyname, taxnumber, phone, address) VALUES ('Last company', '153123123', '+23123123312', 'Middle of the nowhere');

INSERT INTO users(firstname, lastname, midname, email, birthdate, company_id) VALUES ('Aleksei', 'Chernishov', '', 'al@gmail.com', date('11.11.1911'), (SELECT id FROM companies WHERE taxnumber = '123123123' LIMIT 1));
INSERT INTO users(firstname, lastname, midname, email, birthdate, company_id) VALUES ('Some', 'Random', 'Employee', 'some@mail.co', date('12.12.1023'), (SELECT id FROM companies WHERE taxnumber = '123123123' LIMIT 1));
INSERT INTO users(firstname, lastname, midname, email, birthdate, company_id) VALUES ('Not', 'Google', 'Employee', 'some@notgmail.com', date('23.12.1432'), (SELECT id FROM companies WHERE taxnumber = '123123223' LIMIT 1));