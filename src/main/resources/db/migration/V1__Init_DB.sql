DROP TABLE IF EXISTS public.users;
DROP TABLE IF EXISTS public.companies;

CREATE TABLE public.companies(
    id serial primary key,
    companyname varchar(255),
    taxnumber varchar(12),
    phone varchar(255),
    address varchar(255)
);

CREATE TABLE public.users(
    id serial primary key,
    firstname varchar(255),
    lastname varchar(255),
    midname varchar(255),
    email varchar(255),
    birthdate date,
    company_id bigint,
    foreign key (company_id) references companies(id)
);