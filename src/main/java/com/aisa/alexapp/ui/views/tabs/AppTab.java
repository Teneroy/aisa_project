package com.aisa.alexapp.ui.views.tabs;

/**
 * This is a tabs interface that describe behaviour of future tabs components.
 * Class allows us to make different operations with all objects on the page depends on the realization and page status.
 * */
public interface AppTab {

    /**
     * This method allows us to add some post
     * */
    void addPost();

    /**
     * This method allows us delete some post
     * */
    void deletePost();

    /**
     * This method allows us edit some post
     * */
    void editPost();

    /**
     * This method allows us refresh some table with data to present the newest data from db
     * */
    void refreshTable();

    /**
     * This method allows us hide some table with data
     * */
    void hideTable();

    /**
     * This method allows us show some table with data
     * */
    void showTable();

    /**
     * This method allows search and present some data from db
     * */
    void searchPost(String text);
}
