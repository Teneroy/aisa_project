package com.aisa.alexapp.ui.views.tabs;

import com.aisa.alexapp.backend.entities.User;
import com.aisa.alexapp.backend.services.CompanyService;
import com.aisa.alexapp.backend.services.UserService;
import com.aisa.alexapp.ui.views.add.AddUserForm;
import com.aisa.alexapp.ui.views.edit.EditUserForm;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.tabs.Tab;
import com.vaadin.flow.component.tabs.Tabs;

import java.util.Set;

/**
 * This is an implementation of the AppTab that describes user mode behaviour
 * */
public class UserTab extends Tab implements AppTab  {
    private final Dialog addPostDialog = new Dialog();

    private final Dialog editPostDialog = new Dialog();

    private final AddUserForm addPostForm;

    private final EditUserForm editPostForm;

    private Grid<User> userGrid;

    private UserService userService;

    public UserTab(String text, Grid<User> userGrid,
                   UserService userService, CompanyService companyService) {
        super(text);

        this.userGrid = userGrid;
        this.userService = userService;

        configureUserGrid(this.userGrid);

        addPostForm = new AddUserForm(companyService);
        editPostForm = new EditUserForm(companyService);

        addPostDialog.setWidth("600px");
        addPostDialog.setHeight("600px");

        addPostDialog.add(addPostForm);

        editPostDialog.setWidth("600px");
        editPostDialog.setHeight("600px");

        editPostDialog.add(editPostForm);

        addPostForm.addListener(AddUserForm.CreateEvent.class, this::createUser);
        editPostForm.addListener(EditUserForm.EditEvent.class, this::editUser);
    }

    /**
     * This method creates new user. If something went wrong, shows notification
     * */
    private void createUser(AddUserForm.CreateEvent evt) {
        Notification notification;

        if(!userService.addUser(evt.getUser())) {
            notification = new Notification(
                    "Что-то пошло не так. Попробуйте снова или обратитесь в техническую поддержку", 5000);
            notification.open();
            return;
        }

        refreshTable();
        addPostDialog.close();
        addPostForm.refresh();
    }

    /**
     * This method edit new user. If something went wrong, shows notification
     * */
    private void editUser(EditUserForm.EditEvent evt) {
        Notification notification;

        if(!userService.updateUser(evt.getUser())) {
            notification = new Notification(
                    "Что-то пошло не так. Попробуйте снова или обратитесь в техническую поддержку", 5000);
            notification.open();
            return;
        }

        refreshTable();
        editPostDialog.close();
    }

    @Override
    public void addPost() {
        addPostForm.refresh();
        addPostDialog.open();
    }

    @Override
    public void deletePost() {
        Notification notification;
        Set<User> userSet = userGrid.getSelectedItems();

        if(userSet == null || userSet.size() == 0)
            return;

        boolean status = false;

        for(User user : userSet) {
            try {
                status = userService.deleteUser(user);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            break;
        }

        if(!status) {
            notification = new Notification(
                    "Что-то пошло не так. Попробуйте снова или обратитесь в техническую поддержку", 5000);
            notification.open();
            return;
        }

        refreshTable();
    }

    @Override
    public void editPost() {
        Notification notification;
        Set<User> userSet = userGrid.getSelectedItems();

        if(userSet == null || userSet.size() == 0)
            return;

        User user = (User) userSet.toArray()[0];

        editPostForm.fillForm(user);

        editPostDialog.open();
    }

    @Override
    public void refreshTable() {
        userGrid.setItems(userService.getAllUsers());
    }

    @Override
    public void hideTable() {
        userGrid.setVisible(false);
    }

    @Override
    public void showTable() {
        userGrid.setVisible(true);
    }

    @Override
    public void searchPost(String text) {
        try {
            userGrid.setItems(userService.searchUsersByName(text));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Configures user grid. Added columns, headers, and sortable parameters
     * @param grid Grid that we want to configure
     * */
    public static void configureUserGrid(Grid<User> grid) {
        grid.addColumn(User::getBirthDate).setHeader("Дата рождения").setSortable(true);
        grid.addColumn(User::getFirstName).setHeader("Имя").setSortable(true);
        grid.addColumn(User::getLastName).setHeader("Фамилия").setSortable(true);
        grid.addColumn(User::getMidName).setHeader("Отчество").setSortable(true);
        grid.addColumn(User::getCompanyName).setHeader("Компания").setSortable(true);
    }
}
