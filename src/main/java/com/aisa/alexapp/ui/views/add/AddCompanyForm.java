package com.aisa.alexapp.ui.views.add;

import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.ui.views.customelements.phonenumber.PhoneNumberField;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.textfield.NumberField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.shared.Registration;

/**
 * This is an implementation of the form component.
 * Based on this class, you can add some companies with form
 * */
public class AddCompanyForm extends FormLayout {

    @Id("companyName")
    TextField companyName = new TextField("Наименование компании");

    @Id("taxNumber")
    NumberField taxNumber = new NumberField("ИНН");

    @Id("address")
    TextField companyAddress = new TextField("Адрес");

    @Id("phone")
    PhoneNumberField companyPhoneNumber = new PhoneNumberField();

    Button save = new Button("Создать");

    Binder<Company> binder = new BeanValidationBinder<>(Company.class);

    private final Company company = new Company();

    /**
     * Constructor for the AddCompanyForm class
     * */
    public AddCompanyForm() {

        bindFields();

        add(
                companyName,
                taxNumber,
                companyAddress,
                companyPhoneNumber,
                createButtonLayout()
        );
    }

    Component createButtonLayout() {
        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        save.addClickShortcut(Key.ENTER);

        save.addClickListener(click -> validateAndSave());

        binder.addStatusChangeListener(evt -> save.setEnabled(binder.isValid()));

        return new HorizontalLayout(save);
    }

    /**
     * This method validate and save data from form
     * */
    void validateAndSave() {
        try {
            company.setTaxNumber(taxNumber.getValue().toString());
            binder.writeBean(company);
            //company.setTaxNumber(taxNumber.getValue().toString());
            fireEvent(new CreateEvent(this, company));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method defines validation methods and messages. Also, it bind methods to fields
     * */
    void bindFields() {

        binder.forField(companyName)
                // Validator defined based on a lambda and an error message
                .withValidator(
                        name -> name.length() >= 1,
                        "Это поле обязательно для заполнения")
                .bind(Company::getCompanyName, Company::setCompanyName);

        binder.forField(taxNumber)
                // Validator defined based on a lambda and an error message
                .withValidator(
                        name -> {
                            final int length = (name != null ? Long.toString(name.longValue()).length() : 0);
                            return  ( length >= 9) && (length <= 12) ;
                        },
                        "Это поле обязательно для заполнения, заполните корректно")
                .bind(val -> Double.valueOf(val.getTaxNumber()),
                        (v2, v1) -> v2.setTaxNumber(Long.toString(v1.longValue())));

        binder.forField(companyAddress)
                // Validator defined based on a lambda and an error message
                .withValidator(
                        name -> name.length() >= 1,
                        "Это поле обязательно для заполнения")
                .bind(Company::getAddress, Company::setAddress);

        binder.forField(companyPhoneNumber)
                // Validator defined based on a lambda and an error message
                .withValidator(
                        name -> name.length() >= 3,
                        "Это поле обязательно для заполнения")
                .bind(Company::getPhone, Company::setPhone);
    }

    /**
     * This method refresh form fields
     * */
    public void refresh() {
        binder.readBean(null);
    }

    // Events
    public static abstract class AddCompanyFormEvent extends ComponentEvent<AddCompanyForm> {
        private Company company;

        protected AddCompanyFormEvent(AddCompanyForm source, Company company) {
            super(source, false);
            this.company = company;
        }

        public Company getCompany() {
            return company;
        }
    }

    public static class CreateEvent extends AddCompanyForm.AddCompanyFormEvent {
        CreateEvent(AddCompanyForm source, Company company) {
            super(source, company);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }
}
