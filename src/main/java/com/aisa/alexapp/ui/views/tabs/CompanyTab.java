package com.aisa.alexapp.ui.views.tabs;

import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;
import com.aisa.alexapp.backend.services.CompanyService;
import com.aisa.alexapp.ui.views.add.AddCompanyForm;
import com.aisa.alexapp.ui.views.edit.EditCompanyForm;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.tabs.Tab;

import java.util.Set;

/**
 * This is an implementation of the AppTab that describes company mode behaviour
 * */
public class CompanyTab extends Tab implements AppTab {
    private final Dialog addPostDialog = new Dialog();

    private final Dialog editPostDialog = new Dialog();

    private final AddCompanyForm addPostForm = new AddCompanyForm();

    private final EditCompanyForm editPostForm = new EditCompanyForm();

    private Grid<Company> companyGrid;

    private CompanyService companyService;

    public CompanyTab(String text, Grid<Company> companyGrid, CompanyService companyService) {
        super(text);

        this.companyGrid = companyGrid;
        this.companyService = companyService;

        configureCompanyGrid(this.companyGrid);

        addPostDialog.setWidth("600px");
        addPostDialog.setHeight("600px");

        addPostDialog.add(addPostForm);

        editPostDialog.setWidth("600px");
        editPostDialog.setHeight("600px");

        editPostDialog.add(editPostForm);

        addPostForm.addListener(AddCompanyForm.CreateEvent.class, this::createCompany);
        editPostForm.addListener(EditCompanyForm.EditEvent.class, this::editCompany);
    }

    /**
     * This method creates new company. If something went wrong, shows notification
     * */
    public void createCompany(AddCompanyForm.CreateEvent evt) {
        Notification notification;

        if(!companyService.addCompany(evt.getCompany())) {
            notification = new Notification(
                    "Что-то пошло не так. Попробуйте снова или обратитесь в техническую поддержку", 5000);
            notification.open();
            return;
        }

        refreshTable();
        addPostDialog.close();
        addPostForm.refresh();
    }

    /**
     * This method edit new company. If something went wrong, shows notification
     * */
    public void editCompany(EditCompanyForm.EditEvent evt) {
        Notification notification;

        if(!companyService.updateCompany(evt.getCompany())) {
            notification = new Notification(
                    "Что-то пошло не так. Попробуйте снова или обратитесь в техническую поддержку", 5000);
            notification.open();
            return;
        }

        refreshTable();
        editPostDialog.close();
    }

    @Override
    public void addPost() {
        addPostDialog.open();
    }

    @Override
    public void deletePost() {
        Notification notification;
        Set<Company> companySet = companyGrid.getSelectedItems();

        if(companySet == null || companySet.size() == 0)
            return;

        boolean status = false;

        for(Company company : companySet) {
            try {
                status = companyService.deleteCompany(company);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
            break;
        }

        if(!status) {
            notification = new Notification(
                    "Что-то пошло не так. Попробуйте снова или обратитесь в техническую поддержку", 5000);
            notification.open();
            return;
        }

        refreshTable();
    }

    @Override
    public void editPost() {
        Set<Company> companySet = companyGrid.getSelectedItems();

        if(companySet == null || companySet.size() == 0)
            return;

        Company company = (Company) companySet.toArray()[0];

        editPostForm.fillForm(company);
        editPostDialog.open();
    }

    @Override
    public void refreshTable() {
        companyGrid.setItems(companyService.getAllCompanies());
    }

    @Override
    public void hideTable() {
        companyGrid.setVisible(false);
    }

    @Override
    public void showTable() {
        companyGrid.setVisible(true);
    }

    @Override
    public void searchPost(String text) {
        try {
            companyGrid.setItems(companyService.searchCompanyByName(text));
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

    /**
     * Configures company grid. Added columns, headers, and sortable parameters
     * @param grid Grid that we want to configure
     * */
    public static void configureCompanyGrid(Grid<Company> grid) {
        grid.addColumn(Company::getCompanyName).setHeader("Название компании").setSortable(true);
        grid.addColumn(Company::getTaxNumber).setHeader("ИНН").setSortable(true);
        grid.addColumn(Company::getPhone).setHeader("Телефон").setSortable(true);
        grid.addColumn(Company::getAddress).setHeader("Адрес").setSortable(true);
    }
}
