package com.aisa.alexapp.ui.views.main;

import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;
import com.aisa.alexapp.backend.services.CompanyService;
import com.aisa.alexapp.backend.services.UserService;
import com.aisa.alexapp.ui.MainLayout;
import com.aisa.alexapp.ui.views.tabs.AppTab;
import com.aisa.alexapp.ui.views.tabs.CompanyTab;
import com.aisa.alexapp.ui.views.tabs.UserTab;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Main;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.tabs.Tabs;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * This is the main page. It gets two parameters such as companyService and userService.
 * This class creates basic elements and implements main logic of our program.
 * */
@Component
@Route(value = "", layout = MainLayout.class)
@Scope("prototype")
@PageTitle("Employees and companies")
public class MainView extends Main {

    private final CompanyService companyService;

    private final UserService userService;

    private final Tabs tabs;

    private final Dialog deleteConfirmationDialog = new Dialog();

    private final Dialog companyInfoDialog = new Dialog();

    private final Grid<User> companyInfoGrid = new Grid<>();

    private final TextField searchBox = new TextField();

    /**
     * Constructor of the MainView class.
     * @param companyService is the service that implements backend logic of everything that is connected with company
     * @param userService is the service that implements backend logic of everything that is connected with user
     * */
    public MainView(
        CompanyService companyService,
        UserService userService
    ) {
        addClassName("main-page");

        searchBox.setPlaceholder("Поиск...");
        searchBox.setValueChangeMode(ValueChangeMode.EAGER);
        searchBox.addValueChangeListener(this::searchPost);

        this.userService = userService;
        this.companyService = companyService;

        Grid<User> userGrid = new Grid<>();
        userGrid.addClassName("grid-table");
        Grid<Company> companyGrid = new Grid<>();
        companyGrid.addClassName("grid-table");

        companyGrid.addItemClickListener(companyItemClickEvent -> {
            if(companyItemClickEvent.getClickCount() == 2)
                showCompanyInfo(companyItemClickEvent.getItem());
        });

        UserTab userTab = new UserTab("Сотрудники", userGrid, userService, companyService);
        CompanyTab companyTab = new CompanyTab("Компании", companyGrid, companyService);

        userTab.hideTable();
        companyTab.hideTable();

        this.tabs = new Tabs(true, userTab, companyTab);

        tabs.addSelectedChangeListener(this::tabSelectedChange);
        tabs.addClassName("selection-tabs");

        AppTab tab = (AppTab) this.tabs.getSelectedTab();
        tab.refreshTable();
        tab.showTable();

        configureDeleteConfirmationDialog(deleteConfirmationDialog);

        configureCompanyInfoBox();

        add(createButtonLayout(), this.tabs, userGrid, companyGrid, deleteConfirmationDialog, companyInfoDialog);

    }

    /**
     * This method shows the list of company users in dialog window.
     * @param company Company which users we want to show
     * */
    public void showCompanyInfo(Company company) {
        companyInfoDialog.open();
        companyInfoGrid.setItems(companyService.getUsersByCompanyId(company.getId()));
    }

    /**
     * This method add some settings to the company users table and dialog.
     * */
    public void configureCompanyInfoBox() {
        UserTab.configureUserGrid(companyInfoGrid);

        companyInfoDialog.setMaxHeight("500px");
        companyInfoDialog.setWidth("90%");

        companyInfoDialog.add(companyInfoGrid);
    }

    /**
     * This method creates a button group
     * */
    public com.vaadin.flow.component.Component createButtonLayout() {
        Button addPostButton = new Button("Добавить");
        addPostButton.addClickListener(this::showAddDialog);

        Button editPostButton = new Button("Редактировать");
        editPostButton.addClickListener(this::showEditDialog);

        Button deletePostButton = new Button("Удалить");
        deletePostButton.addClickListener(event -> {deleteConfirmationDialog.open();});

        HorizontalLayout layout = new HorizontalLayout(addPostButton, deletePostButton, editPostButton);

        layout.addClassName("button-layout");

        HorizontalLayout layoutTop = new HorizontalLayout(layout, searchBox);

        layoutTop.addClassName("top-layout");

        return layoutTop;
    }

    /**
     * This method search some posts depends on the search bar values and current tab
     * */
    public void searchPost(HasValue.ValueChangeEvent event) {
        AppTab tab = (AppTab) this.tabs.getSelectedTab();
        String request = searchBox.getValue();

        if (request == null || request.isEmpty()) {
            tab.refreshTable();
            return;
        }

        tab.searchPost(request);
    }

    /**
     * This method delete post depends on current tab
     * */
    private void deletePost(ClickEvent<Button> event) {
        AppTab tab = (AppTab) this.tabs.getSelectedTab();

        tab.deletePost();
    }

    /**
     * This method show actual information depends on current tab
     * */
    private void tabSelectedChange(Tabs.SelectedChangeEvent event) {
        AppTab previousTab = (AppTab) event.getPreviousTab();
        previousTab.hideTable();

        AppTab currentTab = (AppTab) event.getSelectedTab();
        currentTab.showTable();
        currentTab.refreshTable();
    }

    /**
     * This method edit post(show dialog with form) depends on current tab
     * */
    private void showEditDialog(ClickEvent<Button> event) {
        AppTab tabs = (AppTab) this.tabs.getSelectedTab();

        tabs.editPost();
    }

    /**
     * This method add post(show dialog with form) depends on current tab
     * */
    private void showAddDialog(ClickEvent<Button> event) {
        AppTab tabs = (AppTab) this.tabs.getSelectedTab();

        tabs.addPost();
    }

    /**
     * Configures delete confirmation dialog window. Added some buttons, and css styles
     * @param deleteConfirmationDialog dialog that we want to configure
     * */
    private void configureDeleteConfirmationDialog(Dialog deleteConfirmationDialog) {
        deleteConfirmationDialog.setMaxHeight("300px");
        deleteConfirmationDialog.setMaxWidth("600px");
        Button confirmDeleteButton = new Button("Уверен");
        Button cancelDeleteButton = new Button("Пожалуй не стоит");

        cancelDeleteButton.addClickListener(event -> deleteConfirmationDialog.close());
        confirmDeleteButton.addClickListener(event -> {
            deletePost(event);
            deleteConfirmationDialog.close();
        });

        deleteConfirmationDialog.add(new Paragraph("Вы уверены что хотите удалить этот элемент?"), confirmDeleteButton, cancelDeleteButton);
    }

}
