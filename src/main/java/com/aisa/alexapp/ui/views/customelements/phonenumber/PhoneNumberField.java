package com.aisa.alexapp.ui.views.customelements.phonenumber;

import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextField;

/**
 * This is a custom mobile phone field
 * */
public class PhoneNumberField extends CustomField<String> {
    private final Select countryCode = new Select();
    private final TextField areaCode = new TextField();
    private final TextField subscriberCode = new TextField();

    public PhoneNumberField() {
        subscriberCode.addClassName("phone-code-field");
        setLabel("Phone number");
        countryCode.setItems("+7");
        countryCode.getStyle().set("width", "6em");
        countryCode.setPlaceholder("Code");
        areaCode.setPattern("[0-9]*");
        areaCode.setPreventInvalidInput(true);
        areaCode.setMaxLength(3);
        areaCode.setPlaceholder("Area");
        areaCode.getStyle().set("width", "5em");
        subscriberCode.setPattern("[0-9]*");
        subscriberCode.setPreventInvalidInput(true);
        subscriberCode.setMaxLength(8);
        subscriberCode.setPlaceholder("Subscriber");
        subscriberCode.getStyle().set("width", "100%");
        subscriberCode.getStyle().set("overflow", "auto");
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.add(countryCode, areaCode, subscriberCode);
        add(horizontalLayout);
    }

    @Override
    protected String generateModelValue() {
        return countryCode.getValue() + "" + areaCode.getValue() + ""
                + subscriberCode.getValue();
    }

    @Override
    protected void setPresentationValue(String newPresentationValue) {
        if (newPresentationValue == null) {
            areaCode.setValue("");
            subscriberCode.setValue("");
        }
    }

    @Override
    public String getValue() {
        return generateModelValue();
    }

    @Override
    public void setValue(String value) {
        if(value == null) {
            setPresentationValue(null);
            return;
        }

        countryCode.setValue("+7");

        if (value.length() < 2)
            return;

        areaCode.setValue(value.substring(2, (Math.min(value.length(), 5))));
        areaCode.setInvalid(false);

        subscriberCode.setValue(value.length() > 5 ? value.substring(5) : "");
        subscriberCode.setInvalid(false);
    }


}
