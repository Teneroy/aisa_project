package com.aisa.alexapp.ui.views.add;

import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;
import com.aisa.alexapp.backend.services.CompanyService;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.Key;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.datepicker.DatePicker;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.textfield.EmailField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.ValidationException;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.shared.Registration;

import java.time.LocalDate;

/**
 * This is an implementation of the form component.
 * Based on this class, you can add some users with form
 * */
public class AddUserForm extends FormLayout {

    CompanyService companyService;

    @Id("firstName")
    TextField userName = new TextField("Имя");

    @Id("lastName")
    TextField userLastName = new TextField("Фамилия");

    @Id("midName")
    TextField userMidName = new TextField("Отчество(если есть)");

    ComboBox<Company> companyBox = new ComboBox<>("Наименование компании");

    @Id("email")
    EmailField emailField = new EmailField("Email");

    @Id("birthDate")
    DatePicker birthDate = new DatePicker("Дата рождения");

    @Id("companyId")
    Long companyId = 0L;

    Button save = new Button("Создать");

    Binder<User> binder = new BeanValidationBinder<>(User.class);

    private final User user = new User();

    /**
     * Constructor for the AddUserForm class
     * */
    public AddUserForm(CompanyService companyService) {

        this.companyService = companyService;

        emailField.setClearButtonVisible(true);
        emailField.setErrorMessage("Пожалйста, введите корректный адрес");

        bindFields();

        companyBox.setItems(this.companyService.getAllCompanies());
        companyBox.setItemLabelGenerator(Company::getCompanyName);
        companyBox.addValueChangeListener(event -> {
            if (event.getValue() == null) {
                companyId = null;
            } else {
                companyId = event.getValue().getId();
            }
        });

        add(
                userName,
                userLastName,
                userMidName,
                birthDate,
                companyBox,
                emailField,
                createButtonLayout()
        );
    }

    Component createButtonLayout() {

        save.addThemeVariants(ButtonVariant.LUMO_PRIMARY);

        save.addClickShortcut(Key.ENTER);

        save.addClickListener(click -> validateAndSave());

        binder.addStatusChangeListener(evt -> save.setEnabled(binder.isValid()));

        return new HorizontalLayout(save);
    }

    /**
     * This method defines validation methods and messages. Also, it bind methods to fields
     * */
    private void bindFields() {
        binder.bindInstanceFields(this);

        binder.forField(userName)
                // Validator defined based on a lambda and an error message
                .withValidator(
                        name -> name.length() >= 1,
                        "Это поле обязательно для заполнения")
                .bind(User::getFirstName, User::setFirstName);

        binder.forField(userLastName)
                // Validator defined based on a lambda and an error message
                .withValidator(
                        name -> name.length() >= 1,
                        "Это поле обязательно для заполнения")
                .bind(User::getLastName, User::setLastName);

        binder.forField(emailField)
                // Validator defined based on a lambda and an error message
                .withValidator(
                        name -> name.length() >= 1,
                        "Это поле обязательно для заполнения")
                .withValidator(new EmailValidator("Этот email не очень похож на корректный"))
                .bind(User::getEmail, User::setEmail);

        binder.forField(userMidName)
                // Validator defined based on a lambda and an error message
                .withValidator(
                        name -> true,
                        "")
                .bind(User::getMidName, User::setMidName);
    }

    /**
     * This method validate and save data from form
     * */
    void validateAndSave() {
        try {
            binder.writeBean(user);
            user.setCompanyId(companyId);
            fireEvent(new CreateEvent(this, user));
        } catch (ValidationException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method refresh form fields
     * */
    public void refresh() {
        binder.readBean(null);
        companyBox.setItems(companyService.getAllCompanies());
    }

    // Events
    public static abstract class AddUserFormEvent extends ComponentEvent<AddUserForm> {
        private User user;

        protected AddUserFormEvent(AddUserForm source, User user) {
            super(source, false);
            this.user = user;
        }

        public User getUser() {
            return user;
        }
    }

    public static class CreateEvent extends AddUserFormEvent {
        CreateEvent(AddUserForm source, User user) {
            super(source, user);
        }
    }

    public <T extends ComponentEvent<?>> Registration addListener(Class<T> eventType,
                                                                  ComponentEventListener<T> listener) {
        return getEventBus().addListener(eventType, listener);
    }

}
