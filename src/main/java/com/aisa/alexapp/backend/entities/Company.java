package com.aisa.alexapp.backend.entities;

import javax.validation.constraints.NotNull;

/**
 * This is the company entity that describes the company table in our database.
 * */
public class Company {
    private Long id;

    @NotNull
    private String taxNumber;

    @NotNull
    private String phone;

    @NotNull
    private String address;

    @NotNull
    private String companyName;

    public Company() {

    }

    public Company(Long id, String taxNumber, String phone, String address, String companyName) {
        this.id = id;
        this.address = address;
        this.companyName = companyName;
        this.phone = phone;
        this.taxNumber = taxNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaxNumber() {
        return taxNumber;
    }

    public void setTaxNumber(String taxNumber) {
        this.taxNumber = taxNumber;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }
}
