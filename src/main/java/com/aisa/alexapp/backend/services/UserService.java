package com.aisa.alexapp.backend.services;

import com.aisa.alexapp.backend.dao.UserDAO;
import com.aisa.alexapp.backend.entities.User;
import com.aisa.alexapp.backend.utils.CheckUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class UserService {

    private final UserDAO userRepository;

    private final static Logger LOGGER = Logger.getLogger(CompanyService.class.getName());

    public UserService(UserDAO userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * This method returns the list of all users
     * */
    public List<User> getAllUsers() {
        return userRepository.getAllUsers();
    }

    /**
     * This method searches users by the key word.
     * @param name The key word. Cannot be null. If user's initials contains this string, this user is gonna be appended to the final list.
     * */
    public List<User> searchUsersByName(String name) {
        if(name == null) {
            IllegalArgumentException null_exception = new IllegalArgumentException("name parameter cannot be null");
            LOGGER.info(null_exception.getMessage());
            throw null_exception;
        }

        return userRepository.searchUsersByName(name);
    }

    /**
     * This method finds user by id. Returns user if user was found or null if wasn't
     * @param id User id. This value must be more than a zero. Cannot be null.
     * */
    public User findById(Long id) {
        CheckUtils.checkId(id, LOGGER, null);

        return userRepository.findById(id);
    }

    /**
     * This method creates new user.
     * Returns true if operation was successful. Otherwise, false
     * @param user New user that we want to create. All object fields must be initialized in a valid way(except id and companyName, it does not matter). Cannot be null.
     * */
    public boolean addUser(User user) throws IllegalArgumentException {
        CheckUtils.checkUserIsNull(user, LOGGER);

        CheckUtils.checkUserParameters(user, LOGGER);

        user.setMidName((user.getMidName() == null ? "" : user.getMidName()));

        return userRepository.addUser(user);
    }

    /**
     * This method updates the existing user
     * Returns true if operation was successful. Otherwise, false
     * @param user User that we wand to update. All object fields must be initialized in a valid way(except companyName, it does not matter). Cannot be null.
     * */
    public boolean updateUser(User user) throws IllegalArgumentException {
        CheckUtils.checkUserIsNull(user, LOGGER);

        CheckUtils.checkUserParameters(user, LOGGER);

        CheckUtils.checkId(user.getId(), LOGGER, "user");

        user.setMidName((user.getMidName() == null ? "" : user.getMidName()));

        return userRepository.updateUser(user);
    }

    /**
     * This method delete user
     * Returns true if operation was successful. Otherwise, false
     * @param user User that we wand to delete. All object fields must be initialized in a valid way(except companyName, it does not matter). Cannot be null.
     * */
    public boolean deleteUser(User user) throws IllegalArgumentException {
        CheckUtils.checkUserIsNull(user, LOGGER);

        CheckUtils.checkId(user.getId(), LOGGER, "user");

        return userRepository.deleteUser(user);
    }

}
