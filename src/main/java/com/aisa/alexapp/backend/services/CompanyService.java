package com.aisa.alexapp.backend.services;

import com.aisa.alexapp.backend.dao.CompanyDAO;
import com.aisa.alexapp.backend.dao.postgresql.CompanyDAOImpl;
import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;
import com.aisa.alexapp.backend.utils.CheckUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class CompanyService {

    private final CompanyDAO companyRepository;

    private final static Logger LOGGER = Logger.getLogger(CompanyService.class.getName());

    public CompanyService(CompanyDAO companyRepository) {
        this.companyRepository = companyRepository;
    }

    /**
     * This method returns the list of all companies
     * */
    public List<Company> getAllCompanies() {
        return companyRepository.getAllCompanies();
    }

    /**
     * This method searches companies by the key word.
     * @param name The key word. If user's initials contains this string, this companies is gonna be appended to the final list. Cannot be null.
     * */
    public List<Company> searchCompanyByName(String name) throws IllegalArgumentException {
        if(name == null) {
            IllegalArgumentException null_exception = new IllegalArgumentException("name parameter cannot be null");
            LOGGER.info(null_exception.getMessage());
            throw null_exception;
        }

        return companyRepository.searchCompanyByName(name);
    }

    /**
     * This method finds company by id. Returns company if company was found or null if wasn't
     * @param id Company id. Cannot be null. This value must be more than a zero
     * */
    public Company findById(Long id) throws IllegalArgumentException {
        CheckUtils.checkId(id, LOGGER, null);

        return companyRepository.findById(id);
    }

    /**
     * This method creates new company.
     * Returns true if operation was successful. Otherwise, false
     * @param company New company that we want to create. All object fields must be initialized in a valid way(except id, it does not matter). Cannot be null.
     * */
    public boolean addCompany(Company company) throws IllegalArgumentException {
        CheckUtils.checkCompanyIsNull(company, LOGGER);

        CheckUtils.checkCompanyParameters(company, LOGGER);

        return companyRepository.addCompany(company);
    }

    /**
     * This method updates the existing company
     * Returns true if operation was successful. Otherwise, false
     * @param company Company that we wand to update. All object fields must be initialized in a valid way.
     * */
    public boolean updateCompany(Company company) throws IllegalArgumentException {
        CheckUtils.checkCompanyIsNull(company, LOGGER);

        CheckUtils.checkCompanyParameters(company, LOGGER);

        CheckUtils.checkId(company.getId(), LOGGER, "company");

        return companyRepository.updateCompany(company);
    }

    /**
     * This method delete company
     * Returns true if operation was successful. Otherwise, false
     * @param company Company that we wand to delete. All object fields must be initialized in a valid way.
     * */
    public boolean deleteCompany(Company company) throws IllegalArgumentException {
        CheckUtils.checkCompanyIsNull(company, LOGGER);

        CheckUtils.checkId(company.getId(), LOGGER, "company");

        return companyRepository.deleteCompany(company);
    }

    /**
     * This method returns the list of the company's employees.
     * @param id This is the id of the company which employees we want to get. This value must be more than a zero
     * */
    public List<User> getUsersByCompanyId(Long id) {
        CheckUtils.checkId(id, LOGGER, null);

        return companyRepository.getUsersByCompanyId(id);
    }

}
