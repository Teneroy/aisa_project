package com.aisa.alexapp.backend.utils;

import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;

import java.util.logging.Logger;

public class CheckUtils {
    public static void checkId(Long id, Logger logger, String objectName) throws IllegalArgumentException {
        String name = (objectName == null ? "" : objectName);

        if(id == null) {
            IllegalArgumentException null_exception = new IllegalArgumentException(name + " id cannot be an empty parameter");
            logger.info(null_exception.getMessage());
            throw null_exception;
        }

        if(id <= 0L) {
            IllegalArgumentException id_exception = new IllegalArgumentException(name + " id can't be less than a zero or a zero");
            logger.info(id_exception.getMessage());
            throw id_exception;
        }
    }

    public static void checkCompanyParameters(Company company, Logger logger) throws IllegalArgumentException {
        IllegalArgumentException obj_exception;
        IllegalArgumentException null_exception;
        IllegalArgumentException tax_exception;
        if (company == null) {
            null_exception = new IllegalArgumentException("company argument cannot be null");
            logger.info(null_exception.getMessage());
            throw null_exception;
        }
        if (company.getCompanyName() == null || company.getCompanyName().isEmpty()) {
            obj_exception = new IllegalArgumentException("company argument cannot have an empty name parameter");
            logger.info(obj_exception.getMessage());
            throw obj_exception;
        }
        if (company.getAddress() == null || company.getAddress().isEmpty()) {
            obj_exception = new IllegalArgumentException("company argument cannot have an empty address parameter");
            logger.info(obj_exception.getMessage());
            throw obj_exception;
        }
        if (company.getPhone() == null || company.getPhone().isEmpty()) {
            obj_exception = new IllegalArgumentException("company argument cannot have an empty phone parameter");
            logger.info(obj_exception.getMessage());
            throw obj_exception;
        }
        if (company.getTaxNumber() == null || company.getTaxNumber().length() == 0) {
            obj_exception = new IllegalArgumentException("company argument cannot have an empty tax number parameter");
            logger.info(obj_exception.getMessage());
            throw obj_exception;
        }
        if (company.getTaxNumber().length() > 12 && company.getTaxNumber().length() < 9) {
            tax_exception = new IllegalArgumentException("Incorrect tax number length");
            logger.info(tax_exception.getMessage());
            throw tax_exception;
        }
    }

    public static void checkCompanyIsNull(Company company, Logger logger) throws IllegalArgumentException {
        if (company == null) {
            IllegalArgumentException null_exception = new IllegalArgumentException("company argument cannot be null");
            logger.info(null_exception.getMessage());
            throw null_exception;
        }
    }

    public static void checkUserParameters(User user, Logger logger) throws IllegalArgumentException {
        IllegalArgumentException obj_exception;
        IllegalArgumentException cid_exception;
        if (user.getFirstName() == null || user.getFirstName().isEmpty()) {
            obj_exception = new IllegalArgumentException("user argument cannot have an empty firstName parameter");
            logger.info(obj_exception.getMessage());
            throw obj_exception;
        }
        if (user.getLastName() == null || user.getLastName().isEmpty()) {
            obj_exception = new IllegalArgumentException("user argument cannot have an empty lastName parameter");
            logger.info(obj_exception.getMessage());
            throw obj_exception;
        }
        if (user.getEmail() == null || user.getEmail().isEmpty()) {
            obj_exception = new IllegalArgumentException("user argument cannot have an empty email parameter");
            logger.info(obj_exception.getMessage());
            throw obj_exception;
        }
        if (user.getCompanyId() == null || user.getCompanyId() == 0L) {
            obj_exception = new IllegalArgumentException("user argument cannot have an empty companyId parameter");
            logger.info(obj_exception.getMessage());
            throw obj_exception;
        }
        if(user.getBirthDate() == null) {
            obj_exception = new IllegalArgumentException("user argument cannot have an empty birthDate parameter");
            logger.info(obj_exception.getMessage());
            throw obj_exception;
        }
        if (user.getCompanyId() < 0) {
            cid_exception = new IllegalArgumentException("companyId cannot be negative");
            logger.info(cid_exception.getMessage());
            throw cid_exception;
        }
    }

    public static void checkUserIsNull(User user, Logger logger) throws IllegalArgumentException {
        if (user == null) {
            IllegalArgumentException null_exception = new IllegalArgumentException("user argument cannot be null");
            logger.info(null_exception.getMessage());
            throw null_exception;
        }
    }
}
