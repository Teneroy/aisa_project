package com.aisa.alexapp.backend.dao.mappers;

import com.aisa.alexapp.backend.entities.User;

import org.springframework.jdbc.core.RowMapper;
import javax.swing.tree.TreePath;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

/**
 * This is the row mapper that is responsible for the generation user object with the
 * database field values.
 * It's extremely important class that we use when we are working with the jdbc
 * */
public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(final ResultSet rs, final int rowNum) throws SQLException {
        final User user = new User();

        Date date = (Date) rs.getObject("birthdate");

        user.setId(rs.getLong("id"));
        user.setFirstName(rs.getString("firstname"));
        user.setLastName(rs.getString("lastname"));
        user.setBirthDate(date.toLocalDate());
        user.setMidName(rs.getString("midname"));
        user.setEmail(rs.getString("email"));
        user.setCompanyId(rs.getLong("company_id"));
        user.setCompanyName(rs.getString("companyname"));

        return user;
    }

}
