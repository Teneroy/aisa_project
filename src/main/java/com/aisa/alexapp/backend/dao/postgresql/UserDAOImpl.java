package com.aisa.alexapp.backend.dao.postgresql;

import com.aisa.alexapp.backend.dao.UserDAO;
import com.aisa.alexapp.backend.dao.mappers.UserRowMapper;
import com.aisa.alexapp.backend.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * The implementation of the user data abstract object.
 * */
@Repository
public class UserDAOImpl implements UserDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final static Logger LOGGER = Logger.getLogger(UserDAOImpl.class.getName());

    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<User> getAllUsers() {
        UserRowMapper mapper = new UserRowMapper();
        List<User> list;

        String sqlQuery = "SELECT users.id as id, firstname, lastname, birthdate, midname, email, company_id, companyname " +
                "FROM users JOIN companies c on users.company_id = c.id ORDER BY id DESC;";

        try {
            list = namedParameterJdbcTemplate.query(sqlQuery, mapper);
        } catch (DataAccessException e) {
            list = new ArrayList<>();
            LOGGER.info(e.getMessage());
        }

        return list;
    }

    @Override
    public List<User> searchUsersByName(String name) {
        UserRowMapper mapper = new UserRowMapper();
        Map<String, Object> params = new HashMap<>();
        List<User> list;

        params.put("name", name);

        String sqlQuery = "SELECT users.id as id, firstname, lastname, birthdate, midname, email, company_id, companyname " +
                "FROM users JOIN companies c on users.company_id = c.id " +
                "WHERE lower(firstname) LIKE concat('%', lower(:name), '%')" +
                "OR lower(lastname) LIKE concat('%', lower(:name), '%')" +
                "OR lower(midname) LIKE concat('%', lower(:name), '%') ORDER BY id DESC;";

        try {
            list = namedParameterJdbcTemplate.query(sqlQuery, params, mapper);
        } catch (DataAccessException e) {
            list = new ArrayList<>();
            LOGGER.info(e.getMessage());
        }

        return list;
    }

    @Override
    public User findById(Long id) {
        UserRowMapper mapper = new UserRowMapper();
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        String sqlQuery = "SELECT users.id as id, firstname, lastname, birthdate, midname, email, company_id, companyname " +
                "FROM users JOIN companies c on users.company_id = c.id " +
                "WHERE users.id = :id;";

        try {
            return namedParameterJdbcTemplate.queryForObject(sqlQuery, params, mapper);
        } catch (DataAccessException e) {
            LOGGER.info(e.getMessage());
            return null;
        }

    }

    @Override
    public boolean addUser(User user) {
        Map<String, Object> params = new HashMap<>();
        int affectedRows;

        params.put("firstname", user.getFirstName());
        params.put("lastname", user.getLastName());
        params.put("midname", user.getMidName());
        params.put("email", user.getEmail());
        params.put("birthdate", user.getBirthDate());
        params.put("company_id", user.getCompanyId());

        String sqlQuery = "INSERT INTO users (firstname, lastname, midname, email, birthdate, company_id)\n" +
                "VALUES(:firstname, :lastname, :midname, :email, :birthdate, :company_id);";

        try {
            affectedRows = namedParameterJdbcTemplate.update(sqlQuery, params);
            return (affectedRows == 1);
        } catch (DataAccessException e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean updateUser(User user) {
        Map<String, Object> params = new HashMap<>();
        int affectedRows;

        params.put("id", user.getId());
        params.put("firstname", user.getFirstName());
        params.put("lastname", user.getLastName());
        params.put("midname", user.getMidName());
        params.put("email", user.getEmail());
        params.put("birthdate", user.getBirthDate());
        params.put("company_id", user.getCompanyId());

        String sqlQuery = "UPDATE users " +
                "SET firstname = :firstname, lastname = :lastname, midname = :midname, " +
                "email = :email, birthdate = date(:birthdate), company_id = :company_id " +
                "WHERE id = :id;";

        try {
            affectedRows = namedParameterJdbcTemplate.update(sqlQuery, params);
            return (affectedRows == 1);
        } catch (DataAccessException e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean deleteUser(User user) {
        Map<String, Object> params = new HashMap<>();
        int affectedRows;

        params.put("id", user.getId());

        String sqlQuery = "DELETE FROM users WHERE id = :id;";

        try {
            affectedRows = namedParameterJdbcTemplate.update(sqlQuery, params);
            return (affectedRows == 1);
        } catch (DataAccessException e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }
}
