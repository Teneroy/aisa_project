package com.aisa.alexapp.backend.dao;

import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;

import javax.sql.DataSource;
import java.util.List;

/**
 * The interface for the company data abstract object.
 * */
public interface CompanyDAO {

    /**
     * This method allows us to set data source.
     * This method should be autowired.
     * @param dataSource the data source from the spring configuration file
     * */
    void setDataSource(final DataSource dataSource);

    /**
     * This method returns the list of all companies from database
     * */
    List<Company> getAllCompanies();

    /**
     * This method searches companies by the key word.
     * @param name The key word. If user's initials contains this string, this companies is gonna be appended to the final list.
     * */
    List<Company> searchCompanyByName(String name);

    /**
     * This method finds company by id. Returns company if company was found or null if wasn't
     * @param id Company id
     * */
    Company findById(Long id);

    /**
     * This method creates new company in the database.
     * Returns true if operation was successful. Otherwise, false
     * @param company New company that we want to save
     * */
    boolean addCompany(Company company);

    /**
     * This method update the existing company in the database
     * Returns true if operation was successful. Otherwise, false
     * @param company Company that we wand to update in the db
     * */
    boolean updateCompany(Company company);

    /**
     * This method delete company from the database
     * Returns true if operation was successful. Otherwise, false
     * @param company Company that we wand to delete from db
     * */
    boolean deleteCompany(Company company);

    /**
     * This method returns the list of the company's employees.
     * @param id This is the id of the company which employees we want to get
     * */
    List<User> getUsersByCompanyId(Long id);

}
