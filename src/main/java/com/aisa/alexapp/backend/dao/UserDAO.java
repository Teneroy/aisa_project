package com.aisa.alexapp.backend.dao;

import com.aisa.alexapp.backend.entities.User;

import javax.sql.DataSource;
import java.util.List;

/**
 * The interface for the user data abstract object.
 * */
public interface UserDAO {

    /**
     * This method allows us to set data source.
     * This method should be autowired.
     * @param dataSource the data source from the spring configuration file
     * */
    void setDataSource(final DataSource dataSource);

    /**
     * This method returns the list of all users from database
     * */
    List<User> getAllUsers();

    /**
     * This method searches users by the key word.
     * @param name The key word. If user's initials contains this string, this user is gonna be appended to the final list.
     * */
    List<User> searchUsersByName(String name);

    /**
     * This method is finding user by id
     * @param id User id. Returns user if user was found or null if wasn't
     * */
    User findById(Long id);

    /**
     * This method creates new user in the database
     * Returns true if operation was successful. Otherwise, false
     * @param user New user that we want to save
     * */
    boolean addUser(User user);

    /**
     * This method update the existing user in the database
     * Returns true if operation was successful. Otherwise, false
     * @param user User that we wand to update in the db
     * */
    boolean updateUser(User user);

    /**
     * This method delete user from the database
     * Returns true if operation was successful. Otherwise, false
     * @param user User that we wand to delete from db
     * */
    boolean deleteUser(User user);

}
