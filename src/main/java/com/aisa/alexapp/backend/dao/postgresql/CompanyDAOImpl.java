package com.aisa.alexapp.backend.dao.postgresql;

import com.aisa.alexapp.backend.dao.CompanyDAO;
import com.aisa.alexapp.backend.dao.mappers.CompanyRowMapper;
import com.aisa.alexapp.backend.dao.mappers.UserRowMapper;
import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * The implementation of the company data abstract object.
 * */
@Repository
public class CompanyDAOImpl implements CompanyDAO {

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final static Logger LOGGER = Logger.getLogger(CompanyDAOImpl.class.getName());

    @Override
    @Autowired
    public void setDataSource(DataSource dataSource) {
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    @Override
    public List<Company> getAllCompanies() {
        CompanyRowMapper mapper = new CompanyRowMapper();
        List<Company> list;

        String sqlQuery = "SELECT * FROM companies ORDER BY id DESC;";

        try {
            list = namedParameterJdbcTemplate.query(sqlQuery, mapper);
        } catch (DataAccessException e) {
            LOGGER.info(e.getMessage());
            list = new ArrayList<>();
        }

        return list;
    }

    @Override
    public List<Company> searchCompanyByName(String name) {
        CompanyRowMapper mapper = new CompanyRowMapper();
        Map<String, Object> params = new HashMap<>();
        List<Company> list;

        params.put("name", name);

        String sqlQuery = "SELECT * FROM companies WHERE lower(companyname) LIKE concat('%', lower(:name), '%') " +
                "ORDER BY id DESC;";

        try {
            list = namedParameterJdbcTemplate.query(sqlQuery, params, mapper);
        } catch (DataAccessException e) {
            LOGGER.info(e.getMessage());
            list = new ArrayList<>();
        }
        return list;
    }

    @Override
    public Company findById(Long id) {
        CompanyRowMapper mapper = new CompanyRowMapper();
        Map<String, Object> params = new HashMap<>();

        params.put("id", id);

        String sqlQuery = "SELECT * FROM companies WHERE id = :id;";

        try {
            return namedParameterJdbcTemplate.queryForObject(sqlQuery, params, mapper);
        } catch (EmptyResultDataAccessException e) {
            LOGGER.info(e.getMessage());
            return null;
        }
    }

    @Override
    public boolean addCompany(Company company) {
        Map<String, Object> params = new HashMap<>();
        int affectedRows;

        params.put("companyname", company.getCompanyName());
        params.put("taxnumber", company.getTaxNumber());
        params.put("phone", company.getPhone());
        params.put("address", company.getAddress());

        String sqlQuery = "INSERT INTO companies(companyname, taxnumber, phone, address) VALUES (:companyname, :taxnumber, :phone, :address);";

        try {
            affectedRows = namedParameterJdbcTemplate.update(sqlQuery, params);
            return (affectedRows == 1);
        } catch (DataAccessException e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean updateCompany(Company company) {
        Map<String, Object> params = new HashMap<>();
        int affectedRows;

        params.put("id", company.getId());
        params.put("companyname", company.getCompanyName());
        params.put("taxnumber", company.getTaxNumber());
        params.put("phone", company.getPhone());
        params.put("address", company.getAddress());

        String sqlQuery = "UPDATE companies SET companyname = :companyname, taxnumber = :taxnumber, phone = :phone, address = :address WHERE id = :id;";

        try {
            affectedRows = namedParameterJdbcTemplate.update(sqlQuery, params);
            return (affectedRows == 1);
        } catch (DataAccessException e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }

    @Override
    public boolean deleteCompany(Company company) {
        Map<String, Object> params = new HashMap<>();
        int affectedRows;

        params.put("id", company.getId());

        String sqlUserQuery = "DELETE FROM users WHERE company_id = :id;";
        String sqlQuery = "DELETE FROM companies WHERE id = :id;";

        try {
            namedParameterJdbcTemplate.update(sqlUserQuery, params);
            affectedRows = namedParameterJdbcTemplate.update(sqlQuery, params);
            return (affectedRows == 1);
        } catch (DataAccessException e) {
            LOGGER.info(e.getMessage());
            return false;
        }
    }

    @Override
    public List<User> getUsersByCompanyId(Long id) {
        UserRowMapper mapper = new UserRowMapper();
        Map<String, Object> params = new HashMap<>();
        List<User> list;

        params.put("id", id);

        String sqlQuery = "SELECT u.id as id, firstname, lastname, midname, email, birthdate, company_id, companyname " +
                "FROM companies JOIN users u on companies.id = u.company_id" +
                " WHERE companies.id = :id;";

        try {
            list = namedParameterJdbcTemplate.query(sqlQuery, params, mapper);
        } catch (DataAccessException e) {
            LOGGER.info(e.getMessage());
            list = new ArrayList<>();
        }

        return list;
    }
}
