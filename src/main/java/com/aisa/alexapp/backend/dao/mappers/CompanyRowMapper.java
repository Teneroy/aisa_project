package com.aisa.alexapp.backend.dao.mappers;

import com.aisa.alexapp.backend.entities.Company;
import com.aisa.alexapp.backend.entities.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This is the row mapper that is responsible for the generation company object with the
 * database field values.
 * It's extremely important class that we use when we are working with the jdbc
 * */
public class CompanyRowMapper implements RowMapper<Company> {
    @Override
    public Company mapRow(final ResultSet rs, final int rowNum) throws SQLException {
        final Company company = new Company();

        company.setId(rs.getLong("id"));
        company.setCompanyName(rs.getString("companyname"));
        company.setAddress(rs.getString("address"));
        company.setPhone(rs.getString("phone"));
        company.setTaxNumber(rs.getString("taxnumber"));

        return company;
    }
}
