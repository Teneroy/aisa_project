# AISA_project

The system that makes connection between employee and a company

## Database installation

Create your data base. Then create tables using scripts from /src/main/resources/db/migration/V1__Init_DB.sql

## Properties
You have too create your application.properties file into the /src/main/resources directory. Here is the code for this file:
```
server.port=8080
# Ensure application is run in Vaadin 14/npm mode
vaadin.compatibilityMode = false
logging.level.org.atmosphere = warn

dblogin = /*enter user's login for the database*/
dbpassword = /*enter database password*/
dbname = /*enter database name*/
dbport = /*enter database port*/
dbhost = /*enter database host*/
dbdriver = org.postgresql.Driver
sqltype = postgresql
```
If you can also create application-test.properties file with same parameters fot the tests and place it to /src/test/resources

## Pack
``
mvn package
``

## Run
``
mvn spring-boot:run
``